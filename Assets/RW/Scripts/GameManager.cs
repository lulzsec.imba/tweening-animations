using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static private GameManager PrivateInstance;
    static public GameManager Instance { get { return PrivateInstance; } }

    public bool gameStarted = false;
    public TextMeshProUGUI scoreDisplay;
    public GameObject Cog;
    public GameObject Paddle;
    public GameObject backGround;
    public SpriteRenderer bgSprite;
    private int gameScore = 0;
    public float bgShakeRate = 2.0f;
    public Color color;

    private void Awake()
    {
        if (PrivateInstance != null && PrivateInstance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            PrivateInstance = this;
        }
        bgSprite = backGround.GetComponent<SpriteRenderer>();
        color = bgSprite.color;
    }

    public void IncreaseScore(int value)
    {
        gameScore += value;
        scoreDisplay.text = gameScore.ToString();

        LeanTween.cancel(gameObject);
        scoreDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
        scoreDisplay.transform.localScale = Vector3.one;

        LeanTween.rotateZ(scoreDisplay.gameObject, 15.0f, 0.5f).setEasePunch();
        LeanTween.scaleX(scoreDisplay.gameObject, 1.5f, 0.5f).setEasePunch();

        LeanTween.move(backGround, Random.insideUnitCircle * bgShakeRate, 0.25f).setEasePunch();
        backGround.LeanColor(Color.red, 0.3f).setEasePunch().setOnComplete(ChangeColor);
    }

    public void ResetGame()
    {
        Cog.GetComponent<Rigidbody2D>().position = Vector2.zero;
        Cog.GetComponent<Rigidbody2D>().velocity = Vector2.down * BallScript.BallSpeed;
        Paddle.GetComponent<Rigidbody2D>().position = new Vector2(0, -4);
    }

    public void ChangeColor()
    {
        bgSprite.color = color;
    }
}
