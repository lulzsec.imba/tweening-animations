using UnityEngine;
using DG.Tweening;

public class BallScript : MonoBehaviour
{
    static public float BallSpeed = 5.0f;

    private float hitPosition = 0;
    private Vector2 direction;
    private Rigidbody2D ballRigidbody;

    public SpriteRenderer spriteRenderer;
    public Color color;

    public Ease easeOut = Ease.OutQuart;
    public Ease easeIn = Ease.InQuad;

    public AnimationCurve animation;
    private void Awake()
    {
        ballRigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        ballRigidbody.velocity = Vector2.down * BallSpeed;
        color = spriteRenderer.color;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            hitPosition = (ballRigidbody.position.x - collision.rigidbody.position.x) 
                / collision.collider.bounds.size.x;

            direction = new Vector2(hitPosition, 1).normalized;
            ballRigidbody.velocity = direction * BallSpeed;
        }
        LeanTween.cancel(gameObject);
        //transform.DOScale(new Vector3(1f, 1f, 1f), 0.1f).SetEase(easeOut).OnComplete(OnNormalSize);
        //spriteRenderer.DOColor(Color.yellow, 0.1f).SetEase(easeOut);
        transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        LeanTween.scale(gameObject, new Vector3(1.0f, 1.0f), 1.0f).setEase(animation);
        gameObject.LeanColor(Color.yellow, 0.5f).setEasePunch().setOnComplete(OnNormalize);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameManager.Instance.ResetGame();
    }

    public void OnNormalize()
    {
        gameObject.LeanColor(color, 0.5f).setEasePunch();
    }

    //public void OnNormalSize()
    //{
    //    transform.DOScale(new Vector3(0.4f, 0.4f, 0.4f), 0.1f).SetEase(easeIn);
    //    spriteRenderer.DOColor(color, 0.1f).SetEase(easeIn);
    //}
}

