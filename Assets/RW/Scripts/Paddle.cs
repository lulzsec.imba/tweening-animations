using DG.Tweening;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class Paddle : MonoBehaviour
{
    public float paddleSpeed = 5.0f;

    public Ease setEase;

    private Rigidbody2D paddleRigidbody;
    private Vector2 moveDirection = Vector2.zero;

    private float startY;
    public float offsetY = 0.5f;
    public Ease easeOut = Ease.OutQuart;
    public Ease easeIn = Ease.InQuad;

    void Start()
    {
        startY = transform.position.y;
        paddleRigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        paddleRigidbody.position += new Vector2(moveDirection.x * Time.deltaTime, 0);
    }

    public void OnMove(InputValue input)
    {
         moveDirection = input.Get<Vector2>() * paddleSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Cog"))
        {
            LeanTween.cancel(gameObject);
            LeanTween.moveY(gameObject, transform.position.y - 0.5f, 0.5f).setEaseShake();
            //OnBallHit();
        }
    }

    //private void OnBallHit()
    //{
    //    float time = 0.25f;
    //    transform.DOMoveY(startY - offsetY, time).SetEase(easeOut).OnComplete(BackToStartPosition);
    //}
    //private void BackToStartPosition()
    //{
    //    float time = 0.25f;
    //    transform.DOMoveY(startY, time).SetEase(easeIn);
    //}
}
